/*
 * heatsim.c
 *
 *  Created on: 2011-11-17
 *      Author: francis
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <limits.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>

#include "config.h"
#include "part.h"
#include "grid.h"
#include "cart.h"
#include "image.h"
#include "heat.h"
#include "memory.h"
#include "util.h"

#define PROGNAME "heatsim"
#define DEFAULT_OUTPUT_PPM "heatsim.png"
#define DEFAULT_DIMX 1
#define DEFAULT_DIMY 1
#define DEFAULT_ITER 100
#define MAX_TEMP 1000.0
#define DIM_2D 2

#define DUMP_ADV 0

typedef struct ctx {
    cart2d_t *cart;
    grid_t *global_grid;
    grid_t *curr_grid;
    grid_t *next_grid;
    grid_t *heat_grid;
    int numprocs;
    int rank;
    MPI_Comm comm2d;
    FILE *log;
    int verbose;
    int dims[DIM_2D];
    int isperiodic[DIM_2D];
    int coords[DIM_2D];
    int reorder;
    int north_peer;
    int south_peer;
    int east_peer;
    int west_peer;
    MPI_Datatype vector;
} ctx_t;

typedef struct command_opts {
    int dimx;
    int dimy;
    int iter;
    char *input;
    char *output;
    int verbose;
} opts_t;

static opts_t *global_opts = NULL;
void dump_ctx(ctx_t *ctx);
__attribute__((noreturn)) static void usage(void) {
    fprintf(stderr, PROGNAME " " VERSION " " PACKAGE_NAME "\n");
    fprintf(stderr, "Usage: " PROGNAME " [OPTIONS] [COMMAND]\n");
    fprintf(stderr, "\nOptions:\n");
    fprintf(stderr, "  --help	this help\n");
    fprintf(stderr, "  --iter	number of iterations to perform\n");
    fprintf(stderr, "  --dimx	2d decomposition in x dimension\n");
    fprintf(stderr, "  --dimy	2d decomposition in y dimension\n");
    fprintf(stderr, "  --input  png input file\n");
    fprintf(stderr, "  --output ppm output file\n");
    fprintf(stderr, "\n");
    exit(EXIT_FAILURE);
}

static void dump_opts(struct command_opts *opts) {
    printf("%10s %s\n", "option", "value");
    printf("%10s %d\n", "dimx", opts->dimx);
    printf("%10s %d\n", "dimy", opts->dimy);
    printf("%10s %d\n", "iter", opts->iter);
    printf("%10s %s\n", "input", opts->input);
    printf("%10s %s\n", "output", opts->output);
    printf("%10s %d\n", "verbose", opts->verbose);
}

void default_int_value(int *val, int def) {
    if (*val == 0)
		*val = def;
}

static int parse_opts(int argc, char **argv, struct command_opts *opts) {
    
    int idx;
    int opt;
    int ret = 0;

    struct option options[] = {
		{"help", 0, 0, 'h'},
		{"iter", 1, 0, 'r'},
		{"dimx", 1, 0, 'x'},
		{"dimy", 1, 0, 'y'},
		{"input", 1, 0, 'i'},
		{"output", 1, 0, 'o'},
		{"verbose", 0, 0, 'v'},
		{0, 0, 0, 0}
	};

    memset(opts, 0, sizeof(struct command_opts));

    while ((opt = getopt_long(argc, argv, "hvx:y:l:", options, &idx)) != -1) {
		
		switch (opt) {
			case 'r':
			    opts->iter = atoi(optarg);
			    break;
			case 'y':
			    opts->dimy = atoi(optarg);
			    break;
			case 'x':
			    opts->dimx = atoi(optarg);
			    break;
			case 'i':
			    if (asprintf(&opts->input, "%s", optarg) < 0)
				goto err;
			    break;
			case 'o':
			    if (asprintf(&opts->output, "%s", optarg) < 0)
				goto err;
			    break;
			case 'h':
			    usage();
			    break;
			case 'v':
			    opts->verbose = 1;
			    break;
			default:
			    printf("unknown option %c\n", opt);
			    ret = -1;
			    break;
		}
    }

    /* default values*/
    default_int_value(&opts->iter, DEFAULT_ITER);
    default_int_value(&opts->dimx, DEFAULT_DIMX);
    default_int_value(&opts->dimy, DEFAULT_DIMY);
    if (opts->output == NULL)
	if (asprintf(&opts->output, "%s", DEFAULT_OUTPUT_PPM) < 0)
	    goto err;
    if (opts->input == NULL) {
		fprintf(stderr, "missing input file");
		goto err;
    }

    if (opts->dimx == 0 || opts->dimy == 0) {
		fprintf(stderr,"argument error: dimx and dimy must be greater than 0\n");
		ret = -1;
    }

    if (opts->verbose)
		dump_opts(opts);

    global_opts = opts;
    return ret;
err:
    FREE(opts->input);
    FREE(opts->output);
    return -1;
}

FILE *open_logfile(int rank) {
    char str[255];
    sprintf(str, "out-%d", rank);
    FILE *f = fopen(str, "w+");
    return f;
}

ctx_t *make_ctx() {
    ctx_t *ctx = (ctx_t *)calloc(1, sizeof(ctx_t));
    return ctx;
}

void free_ctx(ctx_t *ctx) {

    if (ctx == NULL)
		return;

    free_grid(ctx->global_grid);
    free_grid(ctx->curr_grid);
    free_grid(ctx->next_grid);
    free_grid(ctx->heat_grid);
    free_cart2d(ctx->cart);

    if (ctx->log != NULL) {
		fflush(ctx->log);
		fclose(ctx->log);
    }
    FREE(ctx);
}

int init_ctx(ctx_t *ctx, opts_t *opts) {
    //TODO("lab3");
    MPI_Comm_size(MPI_COMM_WORLD, &ctx->numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &ctx->rank);

    if (opts->dimx * opts->dimy != ctx->numprocs) {
		fprintf(stderr, "2D decomposition blocks must equal number of process\n");
		goto err;
    }

    ctx->log = open_logfile(ctx->rank);
    ctx->verbose = opts->verbose;
    ctx->dims[0] = opts->dimx;
    ctx->dims[1] = opts->dimy;
    ctx->isperiodic[0] = 1;
    ctx->isperiodic[1] = 1;
    ctx->reorder = 0;
    grid_t *new_grid = NULL;

    /* Create 2D cartesian communicator */
    MPI_Cart_create(MPI_COMM_WORLD, DIM_2D, ctx->dims, ctx->isperiodic, ctx->reorder, &ctx->comm2d);

    // Utiliser MPI_Cart_shift() pour déterminer les voisins
	MPI_Cart_shift(ctx->comm2d, 0, 1, &ctx->west_peer, &ctx->east_peer);
	MPI_Cart_shift(ctx->comm2d, 1, 1, &ctx->north_peer, &ctx->south_peer);

    // Utiliser MPI_Cart_coords() pour déterminer sa position dans le domaine cartésien
    MPI_Cart_coords(ctx->comm2d, ctx->rank, DIM_2D, ctx->coords);

    dump_ctx(ctx);

    /*
	 * FIXME: le processus rank=0 charge l'image du disque
	 * et transfert chaque section aux autres processus
	 */
	const int N_MESSAGES = 4;

	if (ctx->rank == 0) {

		/*
		* Send grid dimensions and data
		* Comment traiter le cas de rank=0 ?
		*/
	
		/* load input image */
		image_t *image = load_png(opts->input);
		if (image == NULL)
			goto err;

		/* select the red channel as the heat source */
		ctx->global_grid = grid_from_image(image, CHAN_RED);

		/* grid is normalized to one, multiply by MAX_TEMP */
		grid_multiply(ctx->global_grid, MAX_TEMP);

		/* 2D decomposition */
		ctx->cart = make_cart2d(ctx->global_grid->width,
					ctx->global_grid->height, opts->dimx, opts->dimy);
		cart2d_grid_split(ctx->cart, ctx->global_grid);
		
		/* Send the grid to each process*/
		if(ctx->numprocs > 1) {

			// Nombre de requêtes
			int nbSends = N_MESSAGES * (ctx->numprocs - 1);
			
			// Allocation des primitives MPI pour la communication
			MPI_Request *req = calloc(nbSends, sizeof(MPI_Request));
			MPI_Status *status = calloc(nbSends, sizeof(MPI_Status));
			
			int process_index;
			for(process_index = 1; process_index < ctx->numprocs; ++process_index) {

				// Recuperation des coordonnées du process
				int processCoordinates[DIM_2D];
				MPI_Cart_coords(ctx->comm2d, process_index, DIM_2D, processCoordinates);

				// Recuperation de la grille
				grid_t *grid = cart2d_get_grid(ctx->cart, processCoordinates[0], processCoordinates[1]);	
				
				// Caching du shift pour ce processus
				int process_shift = N_MESSAGES * (process_index - 1);
				
				// Envoi des tailles et du padding 
				MPI_Isend(&grid->width, 1, MPI_INTEGER, process_index, process_shift, ctx->comm2d, req + process_shift);
				MPI_Isend(&grid->height, 1, MPI_INTEGER, process_index, process_shift + 1, ctx->comm2d, req + process_shift + 1);
				MPI_Isend(&grid->padding, 1 , MPI_INTEGER, process_index, process_shift + 2, ctx->comm2d, req + process_shift + 2);
				
				// Envoi de la grille
				MPI_Isend(grid->dbl, grid->pw * grid->ph, MPI_DOUBLE, process_index, process_shift + 3, ctx->comm2d, req + process_shift + 3);
				
				if (DUMP_ADV) printf("To %d : pw=%d | ph=%d | p=%d\n", process_index ,grid->pw, grid->ph, grid->padding);
			}

			MPI_Waitall(nbSends, req, status);
			free(req);
			free(status);
		}
		
		// Obtenir la grille pour le master (rank 0)
		int master_coords[DIM_2D];
		MPI_Cart_coords(ctx->comm2d, ctx->rank, DIM_2D, master_coords);
		new_grid = cart2d_get_grid(ctx->cart, master_coords[0], master_coords[1]);
		
	} 
	else {		
		/*
		* FIXME: receive dimensions of the grid
		* store into new_grid
		*/
		MPI_Request req[N_MESSAGES];
		MPI_Status status[N_MESSAGES];
		
		// Tag du message est calculé à partir du rank
		int tag = N_MESSAGES * (ctx->rank - 1); 
		int width, height, padding;
		MPI_Irecv(&width, 1, MPI_INTEGER, 0, tag, ctx->comm2d, &req[0]);
		MPI_Irecv(&height, 1, MPI_INTEGER, 0, tag + 1, ctx->comm2d, &req[1]);
		MPI_Irecv(&padding, 1, MPI_INTEGER, 0, tag + 2, ctx->comm2d, &req[2]);
		
		// Barrière afin d'être sur de récupérer les trois valeurs nécessaires
		// qui seront utilisées pour savoir combien d'éléments seront 
		// envoyés
		MPI_Waitall(3, req, status);
		
		if (DUMP_ADV) printf("w=%d | h=%d | p=%d\n", width, height, padding);
		
		new_grid = make_grid(width, height, padding);
		
		int dblSize = new_grid->pw * new_grid->ph;
		MPI_Irecv(new_grid->dbl, dblSize, MPI_DOUBLE, 0, tag + 3, ctx->comm2d, &req[3]);
		
		
		MPI_Wait(req + 3, status + 3);
		
	}

    if (new_grid == NULL)
		goto err;
	if (DUMP_ADV){
		printf("[%d] Coords are [%d][%d]\n",ctx->rank, ctx->coords[0], ctx->coords[1]);
		fprintf(ctx->log, "[%d] Received grid \n", ctx->rank);
		fprintf(ctx->log, "[%d] width=%d | height=%d | pw=%d | ph=%d\n", ctx->rank, new_grid->width, new_grid->height, new_grid->pw, new_grid->ph);
		fdump_grid(new_grid, ctx->log);
	}
    /* set padding required for Runge-Kutta */
    ctx->curr_grid = grid_padding(new_grid, 1);
    ctx->next_grid = grid_padding(new_grid, 1);
    ctx->heat_grid = grid_padding(new_grid, 1);
    
    /* Create type vector to exchange columns */
	MPI_Type_vector(ctx->curr_grid->ph, 1, ctx->curr_grid->pw, MPI_DOUBLE, &ctx->vector);
	MPI_Type_commit(&ctx->vector);
	
    
    return 0;
err:
    return -1;
}

void dump_ctx(ctx_t *ctx) {
    fprintf(ctx->log, "*** CONTEXT ***\n");
    fprintf(ctx->log, "rank=%d\n", ctx->rank);
    fprintf(ctx->log, "north=%d south=%d west=%d east=%d \n",
	    ctx->north_peer, ctx->south_peer,
	    ctx->east_peer, ctx->west_peer);
    fprintf(ctx->log, "***************\n");
}

void exchng2d(ctx_t *ctx) {
    
    /*
	 * Echanger les bordures avec les voisins
	 * 4 echanges doivent etre effectues
	 */
	 
    grid_t *grid = ctx->curr_grid;
    int width = grid->pw;
    int height = grid->ph;
    double* data = grid->dbl;
    MPI_Comm comm = ctx->comm2d;
    MPI_Request req[8];
    MPI_Status status[8];
	
	if (DUMP_ADV){
		//printf("[%d] Coords are [%d][%d]\n",ctx->rank, ctx->coords[0], ctx->coords[1]);
		fprintf(ctx->log, "[%d] Current grid before transfer \n", ctx->rank);
		fprintf(ctx->log, "[%d] width=%d | height=%d | pw=%d | ph=%d\n", ctx->rank, grid->width, grid->height, grid->pw, grid->ph);
		fdump_grid(grid, ctx->log);
	}

	double *offset_recv_north = data;
	double *offset_recv_south = data + width * (height - 1);
	double *offset_recv_east = (data + width - 1);
	double *offset_recv_west = data;

	double *offset_send_north = data + width;
	double *offset_send_south = data + width * (height - 2);
	double *offset_send_east = offset_recv_east - 1;
	double *offset_send_west = data + 1;

	MPI_Irecv(offset_recv_south, width, MPI_DOUBLE, ctx->south_peer, 0, comm, &req[0]);
	MPI_Irecv(offset_recv_north, width, MPI_DOUBLE, ctx->north_peer, 1, comm, &req[1]);
	MPI_Irecv(offset_recv_east, 1, ctx->vector, ctx->east_peer, 2, comm, &req[2]);
	MPI_Irecv(offset_recv_west, 1, ctx->vector, ctx->west_peer, 3, comm, &req[3]);

	MPI_Isend(offset_send_north, width, MPI_DOUBLE, ctx->north_peer, 0, comm, &req[4]);
	MPI_Isend(offset_send_south, width, MPI_DOUBLE, ctx->south_peer, 1, comm, &req[5]);
	MPI_Isend(offset_send_west, 1, ctx->vector, ctx->west_peer, 2, comm, &req[7]);
	MPI_Isend(offset_send_east, 1, ctx->vector, ctx->east_peer, 3, comm, &req[6]);
	
	MPI_Waitall(8, req, status);

	if (DUMP_ADV){
		fprintf(ctx->log, "[%d] Current grid AFTER transfer \n", ctx->rank);
		fprintf(ctx->log, "[%d] width=%d | height=%d | pw=%d | ph=%d\n", ctx->rank, grid->width, grid->height, grid->pw, grid->ph);
		fdump_grid(grid, ctx->log);
	}

	int i;
	for(i = 0; i < 8; ++i) {
		int e = status[i].MPI_ERROR;
		if (e) {
			printf("Error : %d\n", status[i].MPI_ERROR);
		}
	}
}

int gather_result(ctx_t *ctx, opts_t *opts) {

	if (DUMP_ADV){
		fprintf(ctx->log, "[%d] next_grid before padding 0 \n", ctx->rank);
		fprintf(ctx->log, "[%d] width=%d | height=%d | pw=%d | ph=%d\n", ctx->rank, ctx->next_grid->width, ctx->next_grid->height, ctx->next_grid->pw, ctx->next_grid->ph);
		fdump_grid(ctx->next_grid, ctx->log);
	}

    int ret = 0;
    grid_t *local_grid = grid_padding(ctx->next_grid,0);
    grid_t *grid;

	if (local_grid == NULL)
		goto err;

	// Processus maitre reçoit et fusionne les morceaux reçus
	if(ctx->rank == 0) {

		// Combien de processus autre que le maitre
		int nProcess = ctx->numprocs - 1;
		if(nProcess > 0) {

			MPI_Request *req = (MPI_Request *) calloc(nProcess, sizeof(MPI_Request));
        	MPI_Status *status = (MPI_Status *) calloc(nProcess, sizeof(MPI_Status));

			int index;
			for(index = 1; index <= nProcess; ++index) {

				int coordinates[2];
            	MPI_Cart_coords(ctx->comm2d, index, DIM_2D, coordinates);

            	// Récupérer la grille
            	grid = cart2d_get_grid(ctx->cart, coordinates[0], coordinates[1]);

            	// Recevoir la grille du processus de rang index
            	MPI_Irecv(grid->dbl, grid->width * grid->height, MPI_DOUBLE, index, index, ctx->comm2d, req + index - 1);
			}

			// Attendre que tous les processus aient envoyé leur message
			MPI_Waitall(nProcess, req, status);
			free(req);
       		free(status);
		}

   		int masterCoordinates[2];
    	MPI_Cart_coords(ctx->comm2d, 0, DIM_2D, masterCoordinates);
		
    	grid = cart2d_get_grid(ctx->cart, masterCoordinates[0], masterCoordinates[1]);
		
		grid_copy(local_grid, grid);
		cart2d_grid_merge(ctx->cart, ctx->global_grid);

		if (DUMP_ADV){
			printf("Dumping Global Grid \n");
			fprintf(ctx->log, "Dumping Global Grid \n");
			fdump_grid(ctx->global_grid, ctx->log);
		}

	} else {
		// Envoi au processus maitre
        MPI_Request req;
        MPI_Status status;
        grid = grid_padding(ctx->next_grid, 0);
        MPI_Isend(grid->dbl, grid->height * grid->width, MPI_DOUBLE, 0, ctx->rank, ctx->comm2d, &req);
        MPI_Wait(&req, &status);
        free_grid(grid);
	}
	
done:
    free_grid(local_grid);
    return ret;
err:
    ret = -1;
    goto done;
}

int main(int argc, char **argv) {
    
    ctx_t *ctx = NULL;
    int rep, ret;
    opts_t opts;

    if (parse_opts(argc, argv, &opts) < 0) {
		printf("Error while parsing arguments\n");
		usage();
    }

    if (opts.verbose)
		dump_opts(&opts);

    MPI_Init(&argc, &argv);

    ctx = make_ctx();
    if (init_ctx(ctx, &opts) < 0)
		goto err;

    if (opts.verbose)
		dump_ctx(ctx);

    if (ctx->verbose) {
		fprintf(ctx->log, "heat grid\n");
		fdump_grid(ctx->heat_grid, ctx->log);
    }

    for (rep = 0; rep < opts.iter; rep++)
    {
		if (ctx->verbose) {
		    fprintf(ctx->log, "iter %d\n", rep);
		    fprintf(ctx->log, "start\n");
		    fdump_grid(ctx->curr_grid, ctx->log);
		}

		grid_set_min(ctx->heat_grid, ctx->curr_grid);
		if (ctx->verbose) {
		    fprintf(ctx->log, "grid_set_min\n");
		    fdump_grid(ctx->curr_grid, ctx->log);
		}

		exchng2d(ctx);
		if (ctx->verbose) {
		    fprintf(ctx->log, "exchng2d\n");
		    fdump_grid(ctx->curr_grid, ctx->log);
		}

		// next_grid will be overwritten by current grid with
		// a diffusion equation
		heat_diffuse(ctx->curr_grid, ctx->next_grid);
		if (ctx->verbose) {
		    fprintf(ctx->log, "heat_diffuse\n");
		    fdump_grid(ctx->next_grid, ctx->log);
		}

		SWAP(ctx->curr_grid, ctx->next_grid);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    if (gather_result(ctx, &opts) < 0)
		goto err;

    if (ctx->rank == 0) {
		printf("saving...\n");
		if (save_grid_png(ctx->global_grid, opts.output) < 0) {
		    printf("saving failed\n");
		    goto err;
		}
    }
	MPI_Barrier(ctx->comm2d);
    ret = EXIT_SUCCESS;

done:
    MPI_Finalize();
    free_ctx(ctx);
    FREE(opts.input);
    FREE(opts.output);
    return ret;
err:
    MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    ret = EXIT_FAILURE;
    goto done;
}
