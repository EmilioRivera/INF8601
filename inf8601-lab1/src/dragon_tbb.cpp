/*
 * dragon_tbb.c
 *
 *  Created on: 2011-08-17
 *      Author: Francis Giraldeau <francis.giraldeau@gmail.com>
 */

#include <iostream>

extern "C" {
#include "dragon.h"
#include "color.h"
#include "utils.h"
}
#include "dragon_tbb.h"
#include "tbb/tbb.h"
#include "tbb/task_scheduler_init.h"
#include "TidMap.h"
#include <iostream> 
#include <mutex>

using namespace std;
using namespace tbb;

TidMap* tidMap;
std::mutex* mutexCounter;
int rangeCounter = 0;

class DragonLimits 
{
public :
	
	piece_t piece;

	DragonLimits() 
	{
		piece_init(&piece);
	}
	
	DragonLimits(DragonLimits& l, split) 
	{
		piece_init(&piece);
	}
	
	void operator()(const blocked_range<uint64_t>& r) 
	{
        piece_limit(r.begin(), r.end(), &piece);
    }
    
    void join(DragonLimits& rhs) 
    {
		piece_merge(&piece, rhs.piece);
	}
};

class DragonDraw 
{
public:

	draw_data* data_;

	DragonDraw(draw_data& draw) 
	{
		data_ = &draw;
	}
	
	~DragonDraw() 
	{
		data_ = NULL;
	}

	void operator()(const blocked_range<int>& range) const 
	{
		int id = tidMap->getIdFromTid(gettid());
		
		// Increment the number of range and print the start and the end
		mutexCounter->lock();
		++rangeCounter;
		printf("Thread #%d, Start range:%d, End range: %d \n", id, range.begin(), range.end());
		mutexCounter->unlock();
		
		int sectionSize = data_->size / data_->nb_thread;
		int startSection = range.begin() / sectionSize;
		int endSection = range.end() / sectionSize;
		
		for (int section = startSection; section <= endSection; section++) {
			int tSectionBegin = (section * sectionSize < range.begin() ? range.begin() : section * sectionSize);
			int tSectionEnd = ((section + 1) * sectionSize > range.end() ? range.end() : (section + 1) * sectionSize);
			dragon_draw_raw(tSectionBegin, tSectionEnd, data_->dragon, data_->dragon_width, data_->dragon_height, data_->limits, section);	
		}
	}
};

class DragonRender 
{
public:

	draw_data* data;

	DragonRender(draw_data& draw) 
	{
		data = &draw;
	}
	
	~DragonRender() {
		data = NULL;
	}
	
	void operator()(const blocked_range<int>& range) const 
	{
		scale_dragon(
			range.begin(), 
			range.end(), 
			data->image,
			data->image_width,
			data->image_height,
			data->dragon,
			data->dragon_width,
			data->dragon_height,
			data->palette
		);
	}
};

class DragonClear {
public:

	draw_data* data_;

	DragonClear(draw_data& draw) 
	{
		data_ = &draw;
	}
	
	~DragonClear() 
	{
		data_ = NULL;
	}
	
	void operator()(const blocked_range<int>& range) const 
	{
		init_canvas(range.begin(), range.end(), data_->dragon, -1);
	}
};

int dragon_draw_tbb(char **canvas, struct rgb *image, int width, int height, uint64_t size, int nb_thread)
{
	struct draw_data data;
	limits_t limits;
	char *dragon = NULL;
	int dragon_width;
	int dragon_height;
	int dragon_surface;
	int scale_x;
	int scale_y;
	int scale;
	int deltaJ;
	int deltaI;
	
	// For tracking number of threads and range counter
	tidMap = new TidMap(nb_thread);
	mutexCounter = new std::mutex();

	struct palette *palette = init_palette(nb_thread);
	if (palette == NULL)
		return -1;

	task_scheduler_init init(nb_thread);

	/* 1. Calculer les limites du dragon */
	dragon_limits_tbb(&limits, size, nb_thread);

	dragon_width = limits.maximums.x - limits.minimums.x;
	dragon_height = limits.maximums.y - limits.minimums.y;
	dragon_surface = dragon_width * dragon_height;
	scale_x = dragon_width / width + 1;
	scale_y = dragon_height / height + 1;
	scale = (scale_x > scale_y ? scale_x : scale_y);
	deltaJ = (scale * width - dragon_width) / 2;
	deltaI = (scale * height - dragon_height) / 2;

	dragon = (char *) malloc(dragon_surface);
	if (dragon == NULL) {
		free_palette(palette);
		return -1;
	}

	data.nb_thread = nb_thread;
	data.dragon = dragon;
	data.image = image;
	data.size = size;
	data.image_height = height;
	data.image_width = width;
	data.dragon_width = dragon_width;
	data.dragon_height = dragon_height;
	data.limits = limits;
	data.scale = scale;
	data.deltaI = deltaI;
	data.deltaJ = deltaJ;
	data.palette = palette;
	data.tid = (int *) calloc(nb_thread, sizeof(int));

	/* 2. Initialiser la surface : DragonClear */
	parallel_for(blocked_range<int>(0, dragon_surface), DragonClear(data));

	/* 3. Dessiner le dragon : DragonDraw */
	parallel_for(blocked_range<int>(0, size), DragonDraw(data));

	/* 4. Effectuer le rendu final */
	parallel_for(blocked_range<int>(0, height), DragonRender(data));

	init.terminate();
	
	tidMap->dump();
	cout << "Nombre d'intervalles: " << rangeCounter << endl;
	delete tidMap;
	delete mutexCounter;
	
	free_palette(palette);
	FREE(data.tid);
	*canvas = dragon;
	return 0;
}

/*
 * Calcule les limites en terme de largeur et de hauteur de
 * la forme du dragon. Requis pour allouer la matrice de dessin.
 */
int dragon_limits_tbb(limits_t *limits, uint64_t size, int nb_thread)
{
	DragonLimits lim;
	piece_t piece;
	piece_init(&piece);
	
	parallel_reduce(blocked_range<uint64_t>(0, size), lim);
	
	piece = lim.piece;
	
	*limits = piece.limits;
	return 0;
}
