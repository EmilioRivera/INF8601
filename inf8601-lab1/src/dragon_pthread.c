	/*
 * dragon_pthread.c
 *
 *  Created on: 2011-08-17
 *      Author: Francis Giraldeau <francis.giraldeau@gmail.com>
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <pthread.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include "dragon.h"
#include "color.h"
#include "dragon_pthread.h"
#include "utils.h"

pthread_mutex_t mutex_stdout;
pthread_mutex_t draw_mutex;

void printf_threadsafe(char *format, ...)
{
	va_list ap;

	va_start(ap, format);
	pthread_mutex_lock(&mutex_stdout);
	vprintf(format, ap);
	pthread_mutex_unlock(&mutex_stdout);
	va_end(ap);
}

void *dragon_draw_worker(void *data)
{	
	struct draw_data *drawData = (struct draw_data *) data;
	
	// Print thread # and thread id
	printf("Thread #%d, Thread ID: %d \n", drawData->id, gettid());
	
	/* 1. Initialiser la surface */
    int area = (drawData->dragon_width * drawData->dragon_height) / drawData->nb_thread;
	init_canvas(area * drawData->id, area + (area * drawData->id), drawData->dragon, -1);

	// All threads have to wait here
	pthread_barrier_wait(drawData->barrier);

	/* 2. Dessiner le dragon */
	uint64_t start = drawData->id * drawData->size / drawData->nb_thread;
	uint64_t end = (drawData->id + 1) * drawData->size / drawData->nb_thread;
    dragon_draw_raw(start, end, drawData->dragon, drawData->dragon_width, drawData->dragon_height, drawData->limits, drawData->id);
	
	// Print range
	printf("Thread #%d, Start range:%" PRId64 ", End range: %" PRId64 "\n", drawData->id, start, end);
	
	// All threads have to wait here
	pthread_barrier_wait(drawData->barrier);

    int dividedHeight = drawData->image_height / drawData->nb_thread;
	int startScale = dividedHeight * drawData->id;
	int endScale = dividedHeight + (dividedHeight * drawData->id);

	/* 3. Effectuer le rendu final */
	scale_dragon(
		startScale,
		endScale,
		drawData->image,
		drawData->image_width,
		drawData->image_height,
		drawData->dragon,
		drawData->dragon_width,
		drawData->dragon_height, 
		drawData->palette
	);

	return NULL;
}

int dragon_draw_pthread(char **canvas, struct rgb *image, int width, int height, uint64_t size, int nb_thread)
{
	pthread_t *threads = NULL;
	pthread_barrier_t barrier;
	limits_t lim;
	struct draw_data info;
	char *dragon = NULL;
	int scale_x;
	int scale_y;
	struct draw_data *data = NULL;
	struct palette *palette = NULL;
	int ret = 0;
	
	palette = init_palette(nb_thread);
	if (palette == NULL)
		goto err;

	/* 1. Initialiser barrier. */
	pthread_barrier_init(&barrier, NULL, nb_thread);

	if (dragon_limits_pthread(&lim, size, nb_thread) < 0)
		goto err;
	
	info.dragon_width = lim.maximums.x - lim.minimums.x;
	info.dragon_height = lim.maximums.y - lim.minimums.y;

	if ((dragon = (char *) malloc(info.dragon_width * info.dragon_height)) == NULL) {
		printf("malloc error dragon\n");
		goto err;
	}

	if ((data = malloc(sizeof(struct draw_data) * nb_thread)) == NULL) {
		printf("malloc error data\n");
		goto err;
	}

	if ((threads = malloc(sizeof(pthread_t) * nb_thread)) == NULL) {
		printf("malloc error threads\n");
		goto err;
	}

	info.image_height = height;
	info.image_width = width;
	scale_x = info.dragon_width / width + 1;
	scale_y = info.dragon_height / height + 1;
	info.scale = (scale_x > scale_y ? scale_x : scale_y);
	info.deltaJ = (info.scale * width - info.dragon_width) / 2;
	info.deltaI = (info.scale * height - info.dragon_height) / 2;
	info.nb_thread = nb_thread;
	info.dragon = dragon;
	info.image = image;
	info.size = size;
	info.limits = lim;
	info.barrier = &barrier;
	info.palette = palette;

	for(int i = 0; i < nb_thread; i++) {
		
		/* Initialiser le draw data de chacun des threads */
		data[i] = info;
		data[i].id = i;

		/* 2. Lancement du calcul parallèle principal avec draw_dragon_worker */
		if(pthread_create(&threads[i], NULL, dragon_draw_worker, (void*)&data[i]) != 0) {
			printf("pthread create error \n");
			goto err;
		};
	}

	/* 3. Attendre la fin du traitement */
	for(int i = 0; i < nb_thread; i++) {
		pthread_join(threads[i], NULL); 
	}
    
	/* 4. Destruction des variables (à compléter). */ 
	pthread_barrier_destroy(info.barrier);
	goto done;
	
done:
	FREE(data);
	FREE(threads);
	free_palette(palette);
	*canvas = dragon;
	return ret;

err:
	FREE(dragon);
	ret = -1;
	goto done;
}

void *dragon_limit_worker(void *data)
{
	struct limit_data *lim = (struct limit_data *) data;
	piece_limit(lim->start, lim->end, &lim->piece);
	return NULL;
}

/*
 * Calcule les limites en terme de largeur et de hauteur de
 * la forme du dragon. Requis pour allouer la matrice de dessin.
 */
int dragon_limits_pthread(limits_t *limits, uint64_t size, int nb_thread)
{
	int ret = 0;
	pthread_t *threads = NULL;
	struct limit_data *thread_data = NULL;
	piece_t master;

	piece_init(&master);

	/* 1. ALlouer de l'espace pour threads et threads_data. */
	if ((threads = malloc(sizeof(pthread_t) * nb_thread)) == NULL) {
		printf("malloc error threads\n");
		goto err;
	}

	if((thread_data = malloc(sizeof(*thread_data) * nb_thread)) == NULL) {
		printf("malloc thread data\n");
		goto err;
	}
        
	/* Initialiser les threads_data */
	int sizeOfRegion = size / nb_thread;
	for(int i = 0; i < nb_thread; ++i) {
		thread_data[i].id = i;
		thread_data[i].start = (i * sizeOfRegion);
		thread_data[i].end = sizeOfRegion + (i * sizeOfRegion);

		piece_init(&thread_data[i].piece);

		/* 2. Lancement du calcul en parallèle avec dragon_limit_worker. */
		if(pthread_create(&threads[i], NULL, dragon_limit_worker, (void*)&thread_data[i]) != 0) {
			printf("pthread create error \n");
			goto err;
		}
	}

	/* 3. Attendre la fin du traitement. */
	for(int i = 0; i < nb_thread; ++i) {
		pthread_join(threads[i], NULL);
	}
	
	/* Fusioner les morceaux */
	for(int i = 1; i < nb_thread; ++i) {
		piece_merge(&thread_data[0].piece, thread_data[i].piece);
	}
	
	master = thread_data[0].piece;
	goto done;
        
done:
	FREE(threads);
	FREE(thread_data);
	*limits = master.limits;
	return ret;
err:
	ret = -1;
	goto done;
}
